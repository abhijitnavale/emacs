# GNU EMACS
My personalized configuration files for Gnu Emacs.

## What will it setup?
  1. Farmhouse Dark Theme with few customization.  
  2. Project Management with Projectile.  
  3. Ruby syntax highlighting, code navigation and other IDE features.  
  4. Python syntax highlighting and other IDE features.  
  5. Ruby on Rails Integration.  
  6. GIT interface for Emacs using Magit.  
  7. Color Code highlighting with rainbow mode.  
  8. Show full file path and major mode of current buffer on title bar.  
  9. Multiple Cursor edit support.  
  10. Default Fira Code (FiraCode Regular) Font along with Ligatures.  
  11. Various status info related to current buffer and system on mode line.  
  12. Show vertical thin light bar to mark fill column.  
  13. Highlight charatects with red background color if they are placed beyond 80th column in a line.  
  14. and many more...

## Installation and Setup
Current configuration is tested on Ubuntu 20.04 LTS 64 bit and Emacs 26.3.  

### Prerequisites
  1. Linux/Apple MAC OS.  
  2. Modern Computer.  
  3. Internet Connection.  
  4. Free disk space.  
  5. Latest stable version of Gnu Emacs.
  6. Optionally install Ruby, Ruby on Rails, rbenv, Python 2/3 and any other required software.
  
### Manual Setup
  1. Install all prerequisites.  
  2. Download the repository and place `emacs` folder in your home directory.  
  3. Backup existing emacs config file `mv ~/.emacs ~/emacs_original`.
  4. `cd ~/emacs/lisp/` and run `get_icicles.sh` script from `lisp/` folder to download latest ICICLES package. It is already included.
  5. Create a new symlink for emacs config file from downloaded repository `ln -s ~/emacs/emacs ~/.emacs`.  
  6. Start Emacs! It will take some time to install all packages for the first time.  
  TODO add for windows
  7. Enjoy!  

## Custom Key Combinations
  1. `C-x k` close current buffer/file.  
  2. `C-c l` copy current buffer/file full path.  
  3. `M-<up>` move current line above previous line.  
  4. `M-<down>` move current line below next line.  
  5. `C-c s` alphabetically sort selected words. Can be multiple words on same line OR can be multiple lines.  
  6. `[F5]` refresh current buffer/file to load new changes.  
  7. `[F8]` to show/hide file browser.  
  8. `C-c i` increment number under cursor.  
  9. `C-c r` Reduce (decrement) number under cursor.  
  10. `M-x` Helm M-x.  
  11. `C-c f` open file by fuzzy matching name from current project. Uses Helm projectile find.  
  12. `C-c g` find occurence of a string/word by fuzzy matching in any of the file from current project. Uses helm projectile with silversearcher.  

#### Multiple Cursors
  1. `C-c C-e` add cursors to all occurances.  
  2. `C-c n` Mark next one occurance like this.  
  3. `C-c p` Mark previous one occurance like this.  
  4. `C-c a` Mark all occurances like this.  

# License

## MIT License

Copyright (c) 2017-2020 Abhijit Navale

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

## Fira Code Ligature Support
https://github.com/tonsky/FiraCode/issues/211#issuecomment-239058632
