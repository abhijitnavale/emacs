;; -*- emacs-lisp -*-
;;
;; MIT License
;;
;; Copyright (c) 2017 Abhijit Navale
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.
;;
;;; Code:
(global-display-line-numbers-mode 1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198"
    "#657b83"])
 '(ansi-term-color-vector
   [unspecified "#2d2a2e" "#ff6188" "#a9dc76" "#ffd866" "#78dce8"
                "#ab9df2" "#a1efe4" "#fcfcfa"] t)
 '(column-number-mode t)
 '(compilation-message-face 'default)
 '(custom-enabled-themes '(monokai))
 '(custom-safe-themes
   '("c7fd1708e08544d1df2cba59b85bd25263180b19b287489d4f17b9118487e718"
     "d92c1c36a5181cf629749bf6feee1886cf6bce248ab075c9d1b1f6096fea9539"
     "622034e2b06b087d3e950652c93e465f3df6eab50bfdceddaa78077487e9bc24"
     "774218d0781ca9aad07888de412eac35b7920bafc10ecc014ecf493d7a74b310"
     "51ec7bfa54adf5fff5d466248ea6431097f5a18224788d0bd7eb1257a4f7b773"
     "3e200d49451ec4b8baa068c989e7fba2a97646091fd555eca0ee5a1386d56077"
     "d89e15a34261019eec9072575d8a924185c27d3da64899905f8548cbd9491a36"
     "57a29645c35ae5ce1660d5987d3da5869b048477a7801ce7ab57bfb25ce12d3e"
     "833ddce3314a4e28411edf3c6efde468f6f2616fc31e17a62587d6a9255f4633"
     "efcecf09905ff85a7c80025551c657299a4d18c5fcfedd3b2f2b6287e4edd659"
     "830877f4aab227556548dc0a28bf395d0abe0e3a0ab95455731c9ea5ab5fe4e1"
     "285d1bf306091644fb49993341e0ad8bafe57130d9981b680c1dbd974475c5c7"
     "7f1d414afda803f3244c6fb4c2c64bea44dac040ed3731ec9d75275b9e831fe5"
     "fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c"
     "00445e6f15d31e9afaa23ed0d765850e9cd5e929be5e8e63b114a3346236c44c"
     "4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3"
     "524fa911b70d6b94d71585c9f0c5966fe85fb3a9ddd635362bfabd1a7981a307"
     "4c7a1f0559674bf6d5dd06ec52c8badc5ba6e091f954ea364a020ed702665aa1"
     "f490984d405f1a97418a92f478218b8e4bcc188cf353e5dd5d5acd2f8efd0790"
     "35c096aa0975d104688a9e59e28860f5af6bb4459fd692ed47557727848e6dfe"
     "28a104f642d09d3e5c62ce3464ea2c143b9130167282ea97ddcc3607b381823f"
     "2d035eb93f92384d11f18ed00930e5cc9964281915689fa035719cab71766a15"
     "10fef6d73ae453f39c9f325915386d41894870b72926e8e9b0c39d030447b703"
     "603a831e0f2e466480cdc633ba37a0b1ae3c3e9a4e90183833bc4def3421a961"
     "dbf0cd368e568e6139bb862c574c4ad4eec1859ce62bc755d2ef98f941062441"
     "082d681b31ef1b18ac57ea2c67bcd54b6c2d4eda52519c4e3d71f95af5b69c8b"
     "41020bc5a5547dbc5eaf2554d188f516cc5d3b80fd5d5ad804444135a6abf5f4"
     "bc7d4cfb6d4bd7074a39f97f0b8a057c5b651c403950bbbc4ad35a609ad6268a"
     "6940b1c837efb74240fb1e1a86eb334448323e92ca87fc8dd30117e7397a29ef"
     "78e6be576f4a526d212d5f9a8798e5706990216e9be10174e3f3b015b8662e27"
     "f703efe04a108fcd4ad104e045b391c706035bce0314a30d72fbf0840b355c2c"
     "0feb7052df6cfc1733c1087d3876c26c66410e5f1337b039be44cb406b6187c6"
     "27a1dd6378f3782a593cc83e108a35c2b93e5ecc3bd9057313e1d88462701fcd"
     "5f824cddac6d892099a91c3f612fcf1b09bb6c322923d779216ab2094375c5ee"
     "eb122e1df607ee9364c2dfb118ae4715a49f1a9e070b9d2eb033f1cefd50a908"
     "1f35dedbeacbfe9ed72810478836105b5617da67ca27f717a29bbb8087e8a1ba"
     "30b14930bec4ada72f48417158155bc38dd35451e0f75b900febd355cda75c3e"
     "c7eb06356fd16a1f552cfc40d900fe7326ae17ae7578f0ef5ba1edd4fdd09e58"
     "36ca8f60565af20ef4f30783aa16a26d96c02df7b4e54e9900a5138fb33808da"
     "bf798e9e8ff00d4bf2512597f36e5a135ce48e477ce88a0764cfb5d8104e8163"
     "c9ddf33b383e74dac7690255dd2c3dfa1961a8e8a1d20e401c6572febef61045"
     "57e3f215bef8784157991c4957965aa31bac935aca011b29d7d8e113a652b693"
     "5072d2a17fcc18fe86e37876392e0a9d41a1569a29e73de5a656a8fcda2b7177"
     "0cd00c17f9c1f408343ac77237efca1e4e335b84406e05221126a6ee7da28971"
     "fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088"
     "9abe2b502db3ed511fea7ab84b62096ba15a3a71cdb106fd989afa179ff8ab8d"
     "24168c7e083ca0bbc87c68d3139ef39f072488703dcdd82343b8cab71c0f62a7"
     "fb83a50c80de36f23aea5919e50e1bccd565ca5bb646af95729dc8c5f926cbf3"
     "e3a1b1fb50e3908e80514de38acbac74be2eb2777fc896e44b54ce44308e5330"
     "b6269b0356ed8d9ed55b0dcea10b4e13227b89fd2af4452eee19ac88297b0f99"
     "b02eae4d22362a941751f690032ea30c7c78d8ca8a1212fdae9eecad28a3587f"
     "c8b83e7692e77f3e2e46c08177b673da6e41b307805cd1982da9e2ea2e90e6d7"
     "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476"
     "d5b121d69e48e0f2a84c8e4580f0ba230423391a78fcb4001ccb35d02494d79e"
     "f34b107e8c8443fe22f189816c134a2cc3b1452c8874d2a4b2e7bb5fe681a10b"
     "ea489f6710a3da0738e7dbdfc124df06a4e3ae82f191ce66c2af3e0a15e99b90"
     "0e8c264f24f11501d3f0cabcd05e5f9811213f07149e4904ed751ffdcdc44739"
     "a25c42c5e2a6a7a3b0331cad124c83406a71bc7e099b60c31dc28a1ff84e8c04"
     "72c7c8b431179cbcfcea4193234be6a0e6916d04c44405fc87905ae16bed422a"
     "e8825f26af32403c5ad8bc983f8610a4a4786eb55e3a363fa9acb48e0677fe7e"
     "cdd26fa6a8c6706c9009db659d2dffd7f4b0350f9cc94e5df657fa295fffec71"
     "f641bdb1b534a06baa5e05ffdb5039fb265fde2764fbfd9a90b0d23b75f3936b"
     "5d3e0746023fc5e246eb3e0e48c1ccb5ce0387fc4273896c6cf02ee349c2eba8"
     "1c656eb3f6ae6c84ced46282cb4ed697bffe2f6c764bb5a737ed7ca6d068f798"
     default))
 '(display-time-mode t)
 '(fci-rule-color "#383838")
 '(flycheck-pylint-use-symbolic-id t)
 '(flycheck-python-flake8-executable "/usr/local/bin/python3.7")
 '(frame-brackground-mode 'dark)
 '(fringe-mode 10 nil (fringe))
 '(highlight-changes-colors '("#ff8eff" "#ab7eff"))
 '(highlight-tail-colors
   '(("#323342" . 0) ("#63de5d" . 20) ("#4BBEAE" . 30) ("#1DB4D0" . 50)
     ("#9A8F21" . 60) ("#A75B00" . 70) ("#F309DF" . 85)
     ("#323342" . 100)))
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f") ("NEXT" . "#dc752f") ("THEM" . "#2d9574")
     ("PROG" . "#3a81c3") ("OKAY" . "#3a81c3") ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f") ("DONE" . "#42ae2c") ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d") ("HACK" . "#b1951d") ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f") ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f")))
 '(linum-format " %6d ")
 '(magit-diff-use-overlays nil)
 '(main-line-color1 "#222232")
 '(main-line-color2 "#333343")
 '(package-check-signature 'allow-unsigned)
 '(package-selected-packages
   '(0blayout 0x0 afternoon-theme ag ample-theme ample-zen-theme anzu
              autumn-light-theme blackboard-theme bliss-theme
              brainfuck-mode chruby clues-theme coffee-mode
              column-enforce-mode company company-c-headers
              company-dict company-web csv-mode ctags darkokai-theme
              darktooth-theme direx docker dotenv-mode dracula-theme
              e2ansi ecb elfeed elfeed-goodies elfeed-web elm-mode
              elpy exec-path-from-shell farmhouse-theme
              farmhouse-themes fira-code-mode flatland-black-theme
              flatland-theme flx-ido flycheck flycheck-yamllint
              flymake-css flymake-haml flymake-jshint flymake-jslint
              flymake-json flymake-less flymake-python-pyflakes
              flymake-ruby flymake-sass free-keys github-theme
              gnu-elpa-keyring-update gruber-darker-theme haml-mode
              helm-ag helm-projectile highlight-indent-guides
              highlight-indentation importmagic js2-mode js3-mode
              json-mode jsx-mode kubernetes linum magit markdown-mode
              moe-theme monokai-pro-theme monokai-theme
              multiple-cursors neotree nlinum pallet pdf-tools
              poet-theme pretty-mode projectile-rails py-autopep8
              py-isort py-yapf pydoc rainbow-delimiters rainbow-mode
              realgud-byebug realgud-pry rjsx-mode robe rubocop
              sass-mode semi slim-mode smartparens smartscan
              solarized-theme solidity-mode spacemacs-theme
              sr-speedbar sunny-day-theme tide timu-caribbean-theme
              timu-line timu-macos-theme timu-rouge-theme
              timu-spacegrey-theme tss typescript-mode vdiff vue-mode
              web-mode white-sand-theme yaml-mode yaml-tomato
              zeno-theme zweilight-theme))
 '(pdf-view-midnight-colors '("#655370" . "#fbf8ef"))
 '(pos-tip-background-color "#E6DB74")
 '(pos-tip-foreground-color "#242728")
 '(powerline-color1 "#222232")
 '(powerline-color2 "#333343")
 '(require-final-newline nil)
 '(show-paren-mode t)
 '(standard-indent 2)
 '(tool-bar-mode nil)
 '(typescript-indent-level 2)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#ff0066") (40 . "#CF4F1F") (60 . "#C26C0F") (80 . "#E6DB74")
     (100 . "#AB8C00") (120 . "#A18F00") (140 . "#989200")
     (160 . "#8E9500") (180 . "#63de5d") (200 . "#729A1E")
     (220 . "#609C3C") (240 . "#4E9D5B") (260 . "#3C9F79")
     (280 . "#53f2dc") (300 . "#299BA6") (320 . "#2896B5")
     (340 . "#2790C3") (360 . "#06d8ff")))
 '(vc-annotate-very-old-color nil)
 '(warning-suppress-types '((use-package)))
 '(weechat-color-list
   (unspecified "#242728" "#323342" "#F70057" "#ff0066" "#86C30D"
                "#63de5d" "#BEB244" "#E6DB74" "#40CAE4" "#06d8ff"
                "#FF61FF" "#ff8eff" "#00b2ac" "#53f2dc" "#f8fbfc"
                "#ffffff"))
 '(when
      (or (not (boundp 'ansi-term-color-vector))
          (not (facep (aref ansi-term-color-vector 0))))))

;; Start emacs in maximized state
(add-to-list 'initial-frame-alist '(fullscreen . maximized))

;; Disable load average info from mode line
(setq-default display-time-default-load-average nil)

(load-file "~/emacs/encodings")

(setq-default frame-title-format '("%f [%m]"))

(unless (require 'exec-path-from-shell nil 'noerror)
  (exec-path-from-shell-initialize))

(load-file "~/emacs/load_path_and_repositories")

(package-initialize)

;; fix for Emacs can not find nodejs installed by nvm
;; (when (memq window-system '(mac ns x))
;; (exec-path-from-shell-initialize) ;; )

;; fix end
;; Make sure to install Use-Package on new installation.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(load-file "~/emacs/use_packages")

(load-file "~/emacs/custom_scripts")

(load-file "~/emacs/set_modes")

(load-file "~/emacs/custom_key_mappings")

;; (load-theme 'blackboard)

;; Test
(defun dedicate-window ()
  ""
  (interactive)
  (set-window-dedicated-p (get-buffer-window) t)
  )
 
(defun undedicate-window ()
  ""
  (interactive)
  (set-window-dedicated-p (get-buffer-window) nil)
  )

;;; emacs ends here

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :background "#2a2a29" :foreground "#EBEDEF" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight regular :height 180 :width normal :foundry "nil" :family "JetBrains Mono"))))
 '(column-enforce-face ((t (:inherit font-lock-warning-face :background "dark red" :foreground "#FFFFFF" :underline (:color "VioletRed1" :style wave :position nil)))))
 '(custom-visibility ((t (:foreground "yellow1" :underline t :weight normal :height 0.8))))
 '(font-lock-builtin-face ((t (:foreground "LightSalmon1"))))
 '(font-lock-comment-face ((t (:foreground "LightYellow4" :slant italic))))
 '(font-lock-constant-face ((t (:inherit fixed-pitch :foreground "#ff6347" :weight thin :height 1.1))))
 '(font-lock-function-name-face ((t (:inherit fixed-pitch :foreground "green3" :slant normal :weight normal :height 1.1))))
 '(font-lock-keyword-face ((t (:inherit fixed-pitch :foreground "deep pink" :weight bold))))
 '(font-lock-string-face ((t (:inherit fixed-pitch :foreground "aquamarine3"))))
 '(font-lock-type-face ((t (:inherit fixed-pitch :foreground "DarkGoldenrod1"))))
 '(font-lock-variable-name-face ((t (:inherit fixed-pitch :foreground "aquamarine"))))
 '(font-lock-warning-face ((t (:inherit error :background "#373844" :foreground "gold1" :weight bold))))
 '(minibuffer-prompt ((((class color) (min-colors 89)) (:foreground "#999891"))))
 '(org-level-1 ((t (:inherit default :background "#FCFF33" :foreground "#770b0b" :extend nil :overline "#5d5862" :weight normal :height (lambda (_x) (poet-theme--height 1))))))
 '(web-mode-html-attr-name-face ((t (:foreground "green"))))
 '(web-mode-html-tag-face ((t (:foreground "yellow"))))
 '(widget-field ((t (:extend nil :background "dark red" :foreground "#ffffff")))))

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
)
(add-hook 'web-mode-hook  'my-web-mode-hook)

;; (set-frame-font "JetBrains Mono 18" nil t)
(set-frame-font "Fira Code-12" nil t)

;; Keep after theme configuration.
;; https://emacs.stackexchange.com/a/1062
;; Reduced to 10 for 13" Screen.
;; Set back to 12 for 15" Screen.
(let ((faces '(mode-line
               mode-line-buffer-id
               mode-line-emphasis
               mode-line-highlight
               mode-line-inactive)))
     (mapc
      (lambda (face) (set-face-attribute face nil :font "Fira Code-10"))
      faces))

(defun set-newline-and-indent ()
  (local-set-key (kbd "RET") 'newline-and-indent))
(add-hook 'slim-mode-hook 'set-newline-and-indent)
