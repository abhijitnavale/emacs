# MIT License

# Copyright (c) 2017 Abhijit Navale

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#
setup_on_rpm() {
    echo "Processing for RPM based Linux."
    
}

setup_on_deb() {
    echo "Processing for DEB based Linux."
    echo ""
    # common tasks and packages
    # sudo apt-get update
    sudo apt-get install emacs -y
    sudo apt-get install shellcheck -y
    sudo apt-get install silversearcher-ag -y
    sudo apt-get install importmagic -y
    sudo apt-get install libpq-dev -y
    sudo apt-get install git -y
    sudo apt-get install wget -y
    sudo apt-get install curl -y
    # Python Dependencies
    sudo apt-get install ipython -y
    sudo apt-get install python-pip python3-pip -y
    sudo apt-get install flake8 -y
    sudo apt-get install python-autopep8 -y
    sudo apt-get install python-jedi python3-jedi -y
    pip3 install importmagic epc
    # Ruby Depedencies
    # RVM
    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
    # \curl -sSL https://get.rvm.io | bash -s stable --ruby
    source ~/.rvm/scripts/rvm
    # RubyGems
    gem install pry
    gem install rubocop
    gem install pg
    
    echo ""
    echo "Done. Installation Completed!"
}
